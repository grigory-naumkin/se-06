package ru.naumkin.tm.command.task;

import ru.naumkin.tm.command.AbstractCommand;
import ru.naumkin.tm.service.TaskService;

public class TaskClearCommand extends AbstractCommand {

    public TaskClearCommand() {
        super(true);
    }

    @Override
    public String getName() {
        return "task-clear";
    }

    @Override
    public String getDescription() {
        return "Remove all tasks.";
    }

    @Override
    public void execute() throws Exception {
        bootstrap.getView().showMessage("[TASK LIST CLEAR]");
        TaskService taskService = bootstrap.getTaskService();
        taskService.removeAll(bootstrap.getCurrentUser().getID());
        bootstrap.getView().showMessage("[OK]");
    }

}
